import { db, DeviceOrientation, TrackPoint } from '../store/db'
import { log } from '../common/utils'
import { Coordinate } from 'ol/coordinate'
import OLFeature from 'ol/Feature'
import Point from 'ol/geom/Point'
import LineString from 'ol/geom/LineString'
import { fromLonLat, toLonLat } from 'ol/proj'
import { Vector } from 'ol/source'
import OLVectorLayer from 'ol/layer/Vector'
import { Circle, Fill, Stroke, Style } from 'ol/style'
import { Layer } from 'ol/layer'
import { palette, PRECISION } from '../config'
import { LonLat, MapCenter } from '../store/store'
import { getHorizonInfo } from '../header/header.api'

const LON = 0 // Array index
const LAT = 1 // Array index

// Keep a history of the positions we've seen
// If this one is too close to last one, discard it and update the noMotion time
export const updatePositionHistory = async (
  geolocation: GeolocationPosition
) => {
  // Is there an active autoTrack?
  let activeAutoTrack = await getActiveAutoTrack()
  if (!activeAutoTrack) return
  let lastPoint = activeAutoTrack.points[0]
  if (!lastPoint) {
    log('Creating first point for AutoTrack')
    // This is the first point, so just add it and leave
    const firstPoint: TrackPoint = {
      time: Date.now(),
      lonLat: formatLonLat(geolocation.coords),
      accuracy: geolocation.coords.accuracy,
      altitude: geolocation.coords.altitude ?? undefined,
      altitudeAccuracy: geolocation.coords.altitudeAccuracy ?? undefined,
      distanceFromLast: 0,
      timeSinceLast: 0,
      timeNoMotion: 0,
    }
    if (activeAutoTrack?.id) {
      await db.tracks.update(activeAutoTrack.id, { points: [firstPoint] })
    }
    return
  }

  // Update the array
  const newPoint: TrackPoint = {
    time: Date.now(),
    lonLat: formatLonLat(geolocation.coords),
    accuracy: geolocation.coords.accuracy,
    altitude: geolocation.coords.altitude ?? undefined,
    altitudeAccuracy: geolocation.coords.altitudeAccuracy ?? undefined,
    // TODO calculate the 3 values below
    // @ts-ignore
    distanceFromLast: lastPoint.distanceFromLast + 1,
    // @ts-ignore
    timeSinceLast: lastPoint.timeSinceLast + 1,
    // @ts-ignore
    timeNoMotion: lastPoint.timeNoMotion + 1,
  }

  const newPoints = [newPoint, ...activeAutoTrack.points]
  if (activeAutoTrack.id)
    await db.tracks.update(activeAutoTrack.id, { points: newPoints })
}

// Create an autoTrack and update appStatus
const createNewAutoTrack = async () => {
  log('Will create a new active AutoTrack')
  const newAutoTrackId = await db.tracks.add({
    by: 'defaultUser',
    startTime: Date.now(),
    endTime: 0,
    status: 'isAutoTrack',
    points: [],
    isPublic: false,
    hideForMe: false,
    shareWith: [],
  })

  await db.appStatus.update('TrackTracker', {
    activeAutoTrackId: newAutoTrackId,
  })

  return newAutoTrackId
}

const formatLonLat = (coordinates: GeolocationCoordinates) => {
  const devOffset = import.meta.env.DEV ? Math.random() / 100 : 0

  const lon = parseFloat(
    (coordinates.longitude + devOffset).toFixed(PRECISION.formatting)
  )
  const lat = parseFloat(coordinates.latitude.toFixed(PRECISION.formatting))

  return [lon, lat]
}

const findEdgeMarker = (map: any, bearing: number, mapCenter: MapCenter) => {
  // Find the extent (the bounding box)
  const view = map.getView()
  const extent = view.calculateExtent()
  // Note: top and bottom is relative to mid-earth ie [0, 0]
  const minLonLat = toLonLat(extent.slice(0, 2))
  const maxLonLat = toLonLat(extent.slice(2))

  let markerLon = 0
  let markerLat = 0
  /* The calculations below (per quadrant of the map) are very similar but importantly different.
     Creating a generic function for this is not worth it, I think.
     The basis is the trig of the tan(bearing)

     E.g. top right corner:

     deviceBearing
     |    edgeMarker
     |   /
     |  /
     | /
     /
     mapCenter

     In this example, the bearing is almost 360. For a marker on the top edge of the map we need the tan of: (360 - bearing).
     The adjacent (height) of the triangle is from center to top edge of extent, so: maxExtentLat - centerLat.
     The opposite (width) of the triangle is from center to the marker (ie the offset).

     When the marker goes over the right side of screen, the triangle is different.
   */
  if (bearing >= 270) {
    // Top right corner of map
    const offsetLat =
      getTanFromDegrees(bearing - 270) *
      (maxLonLat[LON] - mapCenter.lonLat[LON])
    markerLat = mapCenter.lonLat[LAT] + offsetLat
    if (markerLat <= maxLonLat[LAT]) {
      // Right side of screen (so a lat line)
      markerLon = maxLonLat[LON]
    } else {
      // Top edge of screen (so a lon line)
      const offsetLon =
        getTanFromDegrees(360 - bearing) *
        (maxLonLat[LAT] - mapCenter.lonLat[LAT])
      markerLon = mapCenter.lonLat[LON] + offsetLon
      markerLat = maxLonLat[LAT]
    }
  } else if (bearing >= 180) {
    // Bottom right corner of map
    const offsetLat =
      getTanFromDegrees(270 - bearing) *
      (maxLonLat[LON] - mapCenter.lonLat[LON])
    markerLat = mapCenter.lonLat[LAT] - offsetLat
    if (markerLat >= minLonLat[LAT]) {
      // Right side of screen (so a lat line)
      markerLon = maxLonLat[LON]
    } else {
      // Bottom edge of screen (so a lon line)
      const offsetLon =
        getTanFromDegrees(bearing - 180) *
        (mapCenter.lonLat[LAT] - minLonLat[LAT])
      markerLon = mapCenter.lonLat[LON] + offsetLon
      markerLat = minLonLat[LAT]
    }
  } else if (bearing >= 90) {
    // Bottom left corner of the map
    const offsetLat =
      getTanFromDegrees(bearing - 90) * (mapCenter.lonLat[LON] - minLonLat[LON])
    markerLat = mapCenter.lonLat[LAT] - offsetLat
    if (markerLat >= minLonLat[LAT]) {
      // Left side of screen (so a lat line)
      markerLon = minLonLat[LON]
    } else {
      // Bottom edge of screen (so a lon line)
      const offsetLon =
        getTanFromDegrees(180 - bearing) *
        (mapCenter.lonLat[LAT] - minLonLat[LAT])
      markerLon = mapCenter.lonLat[LON] - offsetLon
      markerLat = minLonLat[LAT]
    }
  } else {
    // Top left corner of map
    const offsetLat =
      getTanFromDegrees(90 - bearing) * (mapCenter.lonLat[LON] - minLonLat[LON])
    markerLat = mapCenter.lonLat[LAT] + offsetLat
    if (markerLat <= maxLonLat[LAT]) {
      // Left side of screen (so a lat line)
      markerLon = minLonLat[LON]
    } else {
      // Top edge of screen (so a lon line)
      const offsetLon =
        getTanFromDegrees(bearing) * (maxLonLat[LAT] - mapCenter.lonLat[LAT])
      markerLon = mapCenter.lonLat[LON] - offsetLon
      markerLat = maxLonLat[LAT]
    }
  }
  return { lon: markerLon, lat: markerLat }
}

// Set the northMarker at edge of map
export const setNorthMarker = (
  map: any,
  mapCenter: MapCenter,
  deviceOrientation: DeviceOrientation | undefined
): void => {
  if (!deviceOrientation) return

  /***** Absolute ****/
  // Find the edge position
  const { lon: northMarkerLon, lat: northMarkerLat } = findEdgeMarker(
    map,
    deviceOrientation.bearing,
    mapCenter
  )

  // Set the dot and tail line
  if (deviceOrientation.isValid) {
    // Create a valid marker
    setDotAt(map, [northMarkerLon, northMarkerLat], 'northMarker')
    const startLineAtLon =
      northMarkerLon - 0.2 * (northMarkerLon - mapCenter.lonLat[LON])
    const startLineAtLat =
      northMarkerLat - 0.2 * (northMarkerLat - mapCenter.lonLat[LAT])
    drawLineFromTo(
      map,
      [startLineAtLon, startLineAtLat],
      [northMarkerLon, northMarkerLat],
      'northMarkerLine'
    )
  } else {
    // Create an invalid marker
    setDotAt(map, [northMarkerLon, northMarkerLat], 'northMarker', 'invalid')
    drawLineFromTo(
      map,
      mapCenter.lonLat,
      mapCenter.lonLat,
      'northMarkerLine',
      'remove'
    )
  }

  /***** Relative ****
   * Remove the relative northmarker for now
  // Find the edge position
  const { lon: northMarkerLonRel, lat: northMarkerLatRel } = findEdgeMarker(
    map,
    deviceOrientation.bearings.relative,
    mapCenter
  )
  console.log('rel', northMarkerLonRel, northMarkerLatRel)

  // Set the dot and tail line
  if (deviceOrientation.bearings.relativeIsValid) {
    // Create a valid marker
    setDotAt(map, [northMarkerLonRel, northMarkerLatRel], 'northMarkerRel')
    const startLineAtLonRel =
      northMarkerLonRel - 0.2 * (northMarkerLonRel - mapCenter.lonLat[LON])
    const startLineAtLatRel =
      northMarkerLatRel - 0.2 * (northMarkerLatRel - mapCenter.lonLat[LAT])
    drawLineFromTo(
      map,
      [startLineAtLonRel, startLineAtLatRel],
      [northMarkerLonRel, northMarkerLatRel],
      'northMarkerLineRel'
    )
  } else {
    // Create an invalid marker
    setDotAt(
      map,
      [northMarkerLonRel, northMarkerLatRel],
      'northMarkerRel',
      'invalid'
    )
    drawLineFromTo(
      map,
      mapCenter.lonLat,
      mapCenter.lonLat,
      'northMarkerLineRel',
      'remove'
    )
  }
  /**/
}

// Set the toMarker and the line at the edge of the map
export const setToMarkers = async (
  map: any,
  mapCenter: MapCenter,
  toLonLat: Coordinate | undefined,
  deviceOrientation: DeviceOrientation | undefined
): Promise<void> => {
  if (!toLonLat) {
    // Remove the previous one
    setDotAt(map, [0, 0], 'to', 'remove')
    setDotAt(map, [0, 0], 'toMarker', 'remove')
    drawLineFromTo(map, [0, 0], [0, 0], 'toMarkerLine', 'remove')
    return
  }
  if (!deviceOrientation) return

  const view = map.getView()

  // The dot at the actual destination
  setDotAt(map, toLonLat, 'to')

  // Create the toMarker at edge of map
  // Get bearing from last known (ie hopefully the current) position to destination
  let activeAutoTrack = await getActiveAutoTrack()
  if (!activeAutoTrack) return
  let lastPoint = activeAutoTrack.points[0]
  const opposite = toLonLat[LAT] - lastPoint.lonLat[LAT]
  const adjacent = toLonLat[LON] - lastPoint.lonLat[LON]
  const toBearingRaw = getArcTanInDegrees(adjacent / opposite)

  // The tan calculatinn can get strange, so ensure the bearing is correct
  const toBearing = (toBearingRaw + (opposite < 0 ? 180 : 0) + 360) % 360

  const markerBearing = (toBearing - deviceOrientation.bearing + 360) % 360
  const markerBearingInverse = 360 - markerBearing

  const { lon: toMarkerLon, lat: toMarkerLat } = findEdgeMarker(
    map,
    markerBearingInverse,
    mapCenter
  )

  setDotAt(
    map,
    [toMarkerLon, toMarkerLat],
    'toMarker',
    deviceOrientation.isValid ? '' : 'invalid'
  )
  const startLineAtLon =
    toMarkerLon - 0.2 * (toMarkerLon - mapCenter.lonLat[LON])
  const startLineAtLat =
    toMarkerLat - 0.2 * (toMarkerLat - mapCenter.lonLat[LAT])
  drawLineFromTo(
    map,
    [startLineAtLon, startLineAtLat],
    [toMarkerLon, toMarkerLat],
    'toMarkerLine',
    deviceOrientation.isValid ? '' : 'remove'
  )
}

// Set the horizon marker
export const setHorizonMarker = async (
  map: any,
  here: LonLat,
  deviceOrientation: DeviceOrientation | undefined
): Promise<void> => {
  if (!deviceOrientation) return

  // Assumimg that 'here' is center of map
  const appStatusDexie = await db.appStatus.get('TrackTracker')
  const horizonInfo = await getHorizonInfo(
    'here',
    appStatusDexie?.userViewHeight || ''
  )

  // Convert distance to Lon Lat points
  const lonLatConstantKm = 111.111 // km
  const deltaLonKm =
    getSinFromDegrees(deviceOrientation.bearing) * horizonInfo.distanceKm
  const deltaLatKm =
    getCosFromDegrees(deviceOrientation.bearing) * horizonInfo.distanceKm
  const deltaLon = deltaLonKm / lonLatConstantKm / getCosFromDegrees(here[LAT])
  const deltaLat = deltaLatKm / lonLatConstantKm
  const horizonLon = here[LON] + deltaLon
  const horizonLat = here[LAT] + deltaLat

  // Set the dot and line
  if (deviceOrientation.isValid) {
    // Create a valid marker
    setDotAt(map, [horizonLon, horizonLat], 'horizonMarker')
    drawLineFromTo(map, here, [horizonLon, horizonLat], 'horizonMarkerLine')
  } else {
    // Create an invalid marker
    setDotAt(map, [horizonLon, horizonLat], 'horizonMarker', 'invalid')
    drawLineFromTo(map, here, here, 'horizonMarkerLine', 'remove')
  }
}

export const getActiveAutoTrack = async () => {
  const appStatusDexie = await db.appStatus.get('TrackTracker')
  let activeAutoTrackId = appStatusDexie?.activeAutoTrackId
  if (!activeAutoTrackId) {
    activeAutoTrackId = (await createNewAutoTrack()) as number
  }
  const activeAutoTrack = await db.tracks.get(activeAutoTrackId)
  return activeAutoTrack
}

// radians = degrees * Pi / 180
const getTanFromDegrees = (degrees: number): number =>
  // tan(90) = infinite - here that is largest integer, and works well
  Math.tan((degrees * Math.PI) / 180)

// degrees = radians * 180 / Pi
const getArcTanInDegrees = (tan: number): number => {
  return (Math.atan(tan) * 180) / Math.PI
}

const getSinFromDegrees = (degrees: number): number =>
  Math.sin((degrees * Math.PI) / 180)

const getCosFromDegrees = (degrees: number): number =>
  Math.cos((degrees * Math.PI) / 180)

// Create or remove a named dot. Will replace previous names
export const setDotAt = (
  map: any,
  lonLat: Coordinate,
  name:
    | 'here'
    | 'location'
    | 'northMarker'
    | 'northMarkerRel'
    | 'to'
    | 'toMarker'
    | 'horizonMarker'
    | '',
  data?: 'invalid' | 'remove' | ''
): void => {
  if (!name) return

  // Create the dot feature (colored dot)
  const dotFeature = new OLFeature({
    name,
    geometry: new Point(fromLonLat(lonLat)),
  })

  // Create the source that holds the dot
  const dotSource = new Vector({
    features: [dotFeature],
  })

  // Create the dot layer
  let color = palette.actionSec
  let zIndex = 3000
  switch (name) {
    case 'here':
      color = palette.action
      zIndex++
      break
    case 'horizonMarker':
      color =
        data === 'invalid'
          ? palette.horizonMarkerInvalid
          : palette.horizonMarker
      zIndex++
      break
    case 'northMarker':
      color =
        data === 'invalid' ? palette.northMarkerInvalid : palette.northMarker
      zIndex++
      break
    case 'northMarkerRel':
      color =
        data === 'invalid'
          ? palette.northMarkerInvalidRel
          : palette.northMarkerRel
      zIndex++
      break
    case 'to':
    case 'toMarker':
      color = data === 'invalid' ? palette.toMarkerInvalid : palette.toMarker
      zIndex++
      break
  }

  const dotLayer = new OLVectorLayer({
    source: dotSource,
    style: new Style({
      image: new Circle({
        radius: 10,
        fill: new Fill({ color }),
      }),
    }),
  })
  dotLayer.set('name', name)
  dotLayer.setZIndex(zIndex)

  // Replace the previous dot
  map.getLayers().forEach((layer: Layer) => {
    const layerName = layer?.get('name')
    if (layerName === name) map.removeLayer(layer)
  })
  if (data !== 'remove') map.addLayer(dotLayer)
}

// Draw or remove a named line between two coords. Will replace previous names
const drawLineFromTo = (
  map: any,
  lonLat1: Coordinate,
  lonLat2: Coordinate,
  name:
    | 'northMarkerLine'
    | 'northMarkerLineRel'
    | 'toMarkerLine'
    | 'horizonMarkerLine'
    | '',
  data?: 'remove' | ''
): void => {
  if (!name) return

  // Create the line feature (colored line)
  const lineFeature = new OLFeature({
    geometry: new LineString([fromLonLat(lonLat1), fromLonLat(lonLat2)]),
  })

  // Create the source that holds the line
  const lineSource = new Vector({
    features: [lineFeature],
  })

  // Create the line layer
  let color = palette.actionSec
  let width = 1
  let zIndex = 3000
  switch (name) {
    case 'northMarkerLine':
      color = palette.northMarker
      width = 2
      zIndex++
      break
    case 'northMarkerLineRel':
      color = palette.northMarkerRel
      width = 2
      zIndex++
      break
    case 'toMarkerLine':
      color = palette.toMarker
      width = 2
      zIndex++
      break
    case 'horizonMarkerLine':
      color = palette.horizonMarker
      width = 2
      zIndex++
      break
  }

  const lineLayer = new OLVectorLayer({
    source: lineSource,
    style: new Style({
      fill: new Fill({ color }),
      stroke: new Stroke({ color, width }),
    }),
  })
  lineLayer.set('name', name)
  lineLayer.setZIndex(zIndex)

  // Replace the previous line
  map.getLayers().forEach((layer: Layer) => {
    const layerName = layer?.get('name')
    if (layerName === name) map.removeLayer(layer)
  })
  if (data !== 'remove') map.addLayer(lineLayer)
}
