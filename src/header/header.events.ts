import { log } from '../common/utils'
import { AppStatus, db, DeviceOrientation } from '../store/db'
import { debounce } from 'lodash'
import isOnline from 'is-online'

// Device orientation events
export const deviceOrientationListeners = (setLogs: any) => {
  // Now only absolute events. Relative events (for iOS) need more understanding: relative to what?
  // They do seem to be fired more often than absolute events
  addEventListener(
    'deviceorientationabsolute',
    debounce((event) => {
      handleDeviceOrientationEvent(event as DeviceOrientationEvent, setLogs)
    }, 500)
  )

  addEventListener(
    'deviceorientation',
    debounce((event) => {
      handleDeviceOrientationEvent(event as DeviceOrientationEvent, setLogs)
    }, 500)
  )
}

const handleDeviceOrientationEvent = async (
  event: DeviceOrientationEvent,
  setLogs: any
) => {
  if (!event) return
  if (!event.absolute) return

  const alpha = (event.alpha && Math.round(event.alpha)) || undefined
  if (alpha === undefined) return

  const bearing = (alpha && (360 - alpha) % 360) ?? '?'
  log(`Orient. ${event.absolute ? 'abs' : 'rel'} event: ${bearing} `)

  const oldStatus = await db.appStatus.get('TrackTracker')
  if (!oldStatus) return
  const newBearings = { ...oldStatus.deviceOrientation.bearings }

  if (event.absolute) {
    // Absolute bearing
    newBearings.absolute = bearing
    newBearings.absoluteIsValid = true
    newBearings.absoluteTime = Date.now()
  } else {
    // Relative bearing
    newBearings.relative = bearing
    newBearings.relativeIsValid = true
    newBearings.relativeTime = Date.now()
  }

  db.appStatus.update('TrackTracker', {
    deviceOrientation: {
      time: Date.now(),
      isValid: true,
      alpha: alpha,
      bearing: bearing,
      bearings: newBearings,
    },
  })
}

// Tile loader events
export const tileLoadListeners = (
  setLogs: any,
  setHasLoadError: any,
  setIsLoading: any,
  loadTimer: any
) => {
  window.addEventListener('tileLoadStart', () => {
    // On start: set flags, clear the last timer, start a new timeout to reset the flag
    // Note the isLoading state is not in AppStatus, because setting AppStatus here causes infinite loop
    setHasLoadError(false)
    setIsLoading(true)
    if (loadTimer.current) clearTimeout(loadTimer.current)
    // @ts-ignore - loadTimer is not a Timeout
    loadTimer.current = setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  })
  window.addEventListener('tileLoadError', () => {
    setHasLoadError(true)
    log(
      'Error while loading the map (tiles). Possibly no internet or tile limit reached.',
      setLogs
    )
  })
}

// 'Get here' events
export const getHereListeners = (
  setGettingPositionError: (param: any) => void,
  setGettingPosition: (param: any) => void,
  setStartCountDown: (param: any) => void,
  setCountDown: (param: any) => void,
  countDown: number,
  startCountDown: boolean
) => {
  window.addEventListener('gettingHereError', () => {
    setGettingPositionError(true)
    setGettingPosition(false)
    setStartCountDown(false)
  })
  window.addEventListener('gettingHereStart', () => {
    setGettingPositionError(false)
    setGettingPosition(true)
    setCountDown(10)
    setStartCountDown(true)
  })
  window.addEventListener('gettingHereEnd', () => {
    setGettingPosition(false)
    setStartCountDown(false)
  })
}

// Stored tiles event listeners
export const storeTilesListeners = (
  setStoredTilesPercentageChanged: (
    value: ((prevState: number) => number) | number
  ) => void
) => {
  // Listen for % changes reported by tile loader
  window.addEventListener('storedTilesPercentageChanged', () => {
    const newPercentage = localStorage.getItem('storedTilesPercentage')
    if (newPercentage) setStoredTilesPercentageChanged(parseInt(newPercentage))
  })
}

// Online events
export const onlineListeners = (appStatusDexie: AppStatus | undefined) => {
  // Determine the online internet status
  isOnline().then((onlineStatus) => {
    if (appStatusDexie?.haveInternet === onlineStatus) return
    db.appStatus.update('TrackTracker', { haveInternet: onlineStatus })
  })
}
