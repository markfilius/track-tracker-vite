import Dexie, { Table } from 'dexie'
import { Coordinate } from 'ol/coordinate'
import { DEFAULT_ZOOM } from '../config'

export interface AppStatus {
  appName: string
  userId: string
  userBodyHeight: number
  userViewHeight: string
  storedPercentage: number
  zoom: number
  hasTileLoadError: boolean
  getHere: 'get' | 'move' | ''
  headerMessage: string
  coordsOnClick: Coordinate
  altitudeOnClick: number
  featuresOnClick: string[]
  autoTrack: 'none' | 'lastOnly' | '10min' | '1hour'
  activeAutoTrackId: number
  haveInternet: boolean
  deviceOrientation: DeviceOrientation
  to?: {
    lonLat?: Coordinate
    // bearing: number,
  }
}

interface LogItemDex {
  id?: number
  repeat: number
  time: string
  data: any
}

interface Tracks {
  id?: number
  by: string
  startTime: number
  endTime: number
  status: 'finished' | 'inProgress' | 'isAutoTrack'
  points: TrackPoint[]
  isPublic: boolean
  hideForMe: boolean
  shareWith: string[]
}

export interface TrackPoint {
  time: number
  lonLat: Coordinate
  accuracy: number
  altitude?: number
  altitudeAccuracy?: number
  distanceFromLast?: number
  timeSinceLast?: number
  timeNoMotion?: number
}

export type SearchedLocation = {
  place_id: string
  name: string
  address_line1: string
  address_line2: string
  lon: number
  lat: number
}

type StoredTile = {
  tileId: string
  tileSrc: string
}

export type DeviceOrientation = {
  alpha: number
  bearing: number
  bearings: {
    absolute: number
    absoluteTime: number
    absoluteIsValid: boolean
    relative: number
    relativeTime: number
    relativeOffset: number
    relativeIsValid: boolean
  }
  time: number
  isValid: boolean
}

export class TrackTrackerDexie extends Dexie {
  appStatus!: Table<AppStatus>
  logItems!: Table<LogItemDex>
  tracks!: Table<Tracks>
  searches!: Table<SearchedLocation>
  locations!: Table<SearchedLocation & { locationId: string }>
  tiles!: Table<StoredTile>

  constructor() {
    super('TrackTrackerDb')
    this.version(12).stores({
      appStatus: 'appName',
      logItems: '++id',
      tracks: '++id, by',
      searches: '++id',
      locations: 'locationId',
      tiles: 'tileId',
    })

    // Initial definition of the tables
    this.on('populate', function (transaction) {
      // @ts-ignore - appStatus is not known on transaction
      transaction.appStatus.add({
        appName: 'TrackTracker',
        userId: 'defaultUser',
        userBodyHeight: 1.8,
        userViewHeight: '',
        zoom: DEFAULT_ZOOM,
        hasTileLoadError: false,
        getHere: '',
        headerMessage: '',
        autoTrack: '10min',
        haveInternet: false,
        coordsOnClick: [0, 0],
        deviceOrientation: {
          alpha: undefined,
          bearing: undefined,
          bearings: {},
          time: 0,
          isValid: false,
        },
      })
    })

    // Reset some statusses when db is ready (ie the app opens)
    this.on('ready', (db) => {
      // @ts-ignore - appStatus unknown on db
      db.appStatus.update('TrackTracker', { headerMessage: '', getHere: '' })
    })
  }
}

export const db = new TrackTrackerDexie()
