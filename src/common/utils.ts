import { db } from '../store/db'
import { LogItem } from '../store/store'

export const makeNewLogItem = (
  logMessage: string,
  logs: LogItem[]
): LogItem[] => {
  const logId = logMessage.slice(0, 100).replaceAll(' ', '')
  const lastLog = logs[0]
  let oldLogs = logs.slice(0, 50) // Max 50 items
  let repeats = 1

  if (lastLog && lastLog.id === logId) {
    oldLogs.shift()
    repeats = lastLog.repeat + 1
  }

  const time = new Date().toTimeString().slice(0, 8)
  const newLogs = [
    {
      id: logId,
      time: time,
      data: logMessage,
      repeat: repeats,
    },
    ...oldLogs,
  ]
  return newLogs
}

export const makeNewSingleLogItem = (logMessage: string) => {
  // const logId = logMessage.slice(0, 100).replaceAll(' ', '')
  // const lastLog = logs[0]
  // let oldLogs = logs.slice(0, 50) // Max 50 items
  // let repeats = 1
  //
  // if (lastLog && lastLog.id === logId) {
  //   oldLogs.shift()
  //   repeats = lastLog.repeat + 1
  // }

  const time = new Date().toTimeString().slice(0, 8)
  return {
    // id: logId,
    time: time,
    data: logMessage,
    repeat: 1, // repeats,
  }
}

let lastMessage = ''
export const log = async (message: string, setter?: any): Promise<void> => {
  console.log('log:', message)

  // TODO The repeat count is incorrect (too low)- due to parallel calls ????
  if (message === lastMessage) {
    // Update the repeat count
    const lastLog = await db.logItems.toCollection().last()
    if (lastLog) db.logItems.update(lastLog, { repeat: lastLog.repeat + 1 })
    return
  }

  // Keep the last message
  lastMessage = message

  // Add to the db
  try {
    const logId = await db.logItems.add(makeNewSingleLogItem(message))
  } catch (error) {
    console.log('dexxie error', error)
  }
}
