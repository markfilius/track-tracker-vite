import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'
import { log } from '../common/utils'

cleanupOutdatedCaches()

// @ts-ignore - WB_MANFIEST does not exist
precacheAndRoute(self.__WB_MANIFEST)

log('service_worker started')

// event to update
self.addEventListener('install', (event) => {
  log('install or update event fired:')
  log(JSON.stringify(event))
})

// access to indexedDB - yes

// // access to GPS
// setInterval(() => {
//   // navigator.geolocation.getCurrentPosition((g) => {
//   //   console.log('getting geolocation from sw', g)
//   log('heartbeat from serviceWorker')
//   // })
// }, 50)

self.addEventListener('offline', (event) => {
  log('OFFline event fired')
})

self.onoffline = () => {
  console.log('Your worker is now OFFline')
}

self.ononline = () => {
  console.log('Your worker is now ONfline')
}
