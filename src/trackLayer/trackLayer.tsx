import { useRecoilState, useSetRecoilState } from 'recoil'
import { logsState, LonLat, MapCenter, mapCenterState } from '../store/store'
import { useLiveQuery } from 'dexie-react-hooks'
import { db } from '../store/db'
import { useEffect } from 'react'
import { log } from '../common/utils'
import {
  setDotAt,
  setHorizonMarker,
  updatePositionHistory,
} from './trackLayer.utils'
import { palette, PRECISION } from '../config'
import { cloneDeep } from 'lodash'

interface TrackLayerProps {
  map: any
  zIndex: number
}

const TrackLayer = (props: TrackLayerProps) => {
  const { map, zIndex } = props
  const setLogs = useSetRecoilState(logsState)
  const [mapCenter, setMapCenter] = useRecoilState(mapCenterState)
  const appStatusDexie = useLiveQuery(() => db.appStatus.toCollection().last())

  /* Heartbeat Interval - not used for now
  useInterval(
    async () => {
      console.log('Heartbeat')
      // On a heartbeat, get the current position
      if (appStatusDexie?.getHere !== 'get')
        await db.appStatus.update('TrackTracker', { getHere: 'get' })
      getOrMoveHere()
    },
    !appStatusDexie || appStatusDexie?.autoTrack === 'none' ? null : 30000
  )
  */

  // Effect to get current position when requested
  useEffect(() => {
    getOrMoveHere()
  }, [appStatusDexie?.getHere])

  const getOrMoveHere = async () => {
    try {
      if (!appStatusDexie || !appStatusDexie?.getHere) return

      // Update the logs
      if (appStatusDexie.getHere === 'move') log(`Moving to current position`)

      // Reset the getHere status (and remember to move)
      const doMove = appStatusDexie.getHere === 'move'
      await db.appStatus.update('TrackTracker', { getHere: '' })

      // Event to the header
      window.dispatchEvent(new Event('gettingHereStart'))

      // Get or move to the actual position
      getPosition(doMove)
    } catch (error) {
      log('Error while getOrMoveHere: ' + JSON.stringify(error))
    }
  }

  const getPosition = (doMove: boolean) => {
    try {
      let positionCount = 0
      const positions: GeolocationPosition[] = []
      const watchId = navigator.geolocation.watchPosition((g) => {
        positions.push(g)
        positionCount++
      })
      setTimeout(() => {
        navigator.geolocation.clearWatch(watchId)
        if (positions.length < 1) {
          window.dispatchEvent(new Event('gettingHereError'))
          return
        }
        if (positions.length === 1) {
          handleNewCurrentPosition(positions[0], doMove)
          return
        }

        // Average the lonLat values
        log(`Found ${positions.length} positions to average`)
        const totals: { [key: string]: { total: number; n: number } } = {
          accuracy: { total: 0, n: 0 },
          altitude: { total: 0, n: 0 },
          longitude: { total: 0, n: 0 },
          latitude: { total: 0, n: 0 },
        }
        for (const position of positions) {
          // @ts-ignore - GeoLocationPostion type doesnt match
          const coords: { [key: string]: number | null } = position.coords
          for (const key of Object.keys(totals)) {
            if (coords[key] !== null) {
              totals[key] = {
                total: totals[key].total + (coords[key] ?? 0),
                n: ++totals[key].n,
              }
            }
          }
        }
        const positionAverage: GeolocationPosition = {
          timestamp: positions[positions.length - 1].timestamp,
          coords: {
            accuracy: totals.accuracy.n
              ? totals.accuracy.total / totals.accuracy.n
              : 0,
            altitude: totals.altitude.n
              ? totals.altitude.total / totals.altitude.n
              : null,
            longitude: totals.longitude.n
              ? totals.longitude.total / totals.longitude.n
              : 0,
            latitude: totals.latitude.n
              ? totals.latitude.total / totals.latitude.n
              : 0,
            altitudeAccuracy:
              positions[positions.length - 1].coords.altitudeAccuracy,
            heading: positions[positions.length - 1].coords.heading,
            speed: positions[positions.length - 1].coords.speed,
          },
        }
        handleNewCurrentPosition(positionAverage, doMove)
      }, 10000)
    } catch (error) {
      log(
        'Error while  getting current position: ' + JSON.stringify(error),
        setLogs
      )
    }
  }

  const handleNewCurrentPosition = async (
    g: GeolocationPosition,
    doMove: boolean
  ) => {
    // Add to position history (ie we have a new position)
    await updatePositionHistory(g)

    /* not using this for now
    const activeAutoTrackId = appStatusDexie?.activeAutoTrackId
    if (!activeAutoTrackId) return
    const activeAutoTrack = await db.tracks.get(activeAutoTrackId)
    const currentLonLat = activeAutoTrack?.points[0].lonLat
    */
    if (!g.coords.longitude || !g.coords.latitude) return
    const currentLonLat = [g.coords.longitude, g.coords.latitude]

    // Update the logs
    log('Got current pos: ' + currentLonLat.toString())
    if (g.coords.altitude)
      log(
        `Got current altitude: ${g.coords.altitude?.toFixed(
          PRECISION.altitude
        )},` +
          (g.coords.altitudeAccuracy
            ? ` [acc: ${g.coords.altitudeAccuracy?.toFixed(
                PRECISION.altitude
              )}]`
            : '')
      )

    // Set the dot and move
    setDotAt(map, currentLonLat, 'here')
    if (doMove) {
      setMapCenter({
        lonLat: currentLonLat,
        dotName: 'here',
        zoomLevel: mapCenter.zoomLevel,
      } as MapCenter)

      // await db.appStatus.update('TrackTracker', {
      //   coordsOnClick: currentLonLat,
      // })
    }

    // Show the horizon
    setHorizonMarker(
      map,
      currentLonLat as LonLat,
      appStatusDexie?.deviceOrientation
    )

    /* Add relevant number of dots- not used for now
    await addAutoTrack(activeAutoTrackId)
    */

    // Clean up
    window.dispatchEvent(new Event('gettingHereEnd'))
  }

  /* Dots as a track - not used for now
  // Add the dots as track
  const addAutoTrack = async (activeAutoTrackId: number) => {
    const activeAutoTrack = await db.tracks.get(activeAutoTrackId)
    const currentLonLat = activeAutoTrack?.points[0].lonLat
    if (!currentLonLat) return

    // Create the here feature (big red dot)
    const hereFeature = new OLFeature({
      geometry: new Point(fromLonLat(currentLonLat)),
    })
    // Create the dots feature (small red dots)
    const dotsFeature = []
    for (const point of activeAutoTrack?.points) {
      let startTime = Date.now() - 5000 // 5 sec
      switch (appStatusDexie?.autoTrack) {
        case '10min':
          startTime = Date.now() - 600000 // 600k ms = 10 min
      }
      if (point.time < startTime) break
      dotsFeature.push(
        new OLFeature({
          geometry: new Point(fromLonLat(point.lonLat)),
        })
      )
    }

    // Create the here feature (big red dot)
    const hereFeature = new OLFeature({
      geometry: new Point(fromLonLat(currentLonLat)),
    })
    // Create the source that holds the dots
    const dotsSource = new Vector({
      features: dotsFeature,
    })

    // Create the here layer
    const hereLayer = new OLVectorLayer({
      source: hereSource,
      style: new Style({
        image: new Circle({
          radius: 10,
          fill: new Fill({ color: 'red' }),
        }),
      }),
    })
    hereLayer.set('name', 'hereLayer')
    hereLayer.setZIndex(zIndex + 1)
    // Create the dotslayer
    const dotsLayer = new OLVectorLayer({
      source: dotsSource,
      style: new Style({
        image: new Circle({
          radius: 5,
          fill: new Fill({ color: 'red' }),
        }),
      }),
    })
    dotsLayer.set('name', 'autoTrackLayer')
    dotsLayer.setZIndex(zIndex)

    // Replace the previous layers
    map.getLayers().forEach((layer: Layer) => {
      const name = layer?.get('name')
      if (name === 'autoTrackLayer' || name === 'hereLayer')
        map.removeLayer(layer)
    })
    map.addLayer(hereLayer)
    map.addLayer(dotsLayer)
  }
  */

  return null
}

export default TrackLayer
