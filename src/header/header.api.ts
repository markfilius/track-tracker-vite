import { AppStatus, db, SearchedLocation } from '../store/db'
import { LonLat } from '../store/store'
import { PRECISION } from '../config'
import { getActiveAutoTrack } from '../trackLayer/trackLayer.utils'

export const fetchLocationCoords = async (
  searchFor: string
): Promise<SearchedLocation[]> => {
  if (!searchFor) return []

  const limitNrResults = 20

  const getParams = []
  getParams.push('limit=' + limitNrResults)
  getParams.push('text=' + searchFor)
  getParams.push('format=json')
  getParams.push('apiKey=64f9d69d596f45bb9ad58613383d94d7')
  getParams.push('filter=countrycode:au')

  const locationInfoJson = await fetch(
    `https://api.geoapify.com/v1/geocode/search?${getParams.join('&')}`,
    { method: 'GET' }
  )
  const locationInfo = await locationInfoJson.json()
  // console.log('search results', locationInfo)
  return locationInfo.results
}

export const storeLonLatToLocation = (lonLats: SearchedLocation[]): void => {
  console.log('lonlats', lonLats)

  lonLats.map((lonLat) => {
    const locationId = `lon:${lonLat.lon.toFixed(
      PRECISION.locations
    )},lat:${lonLat.lat.toFixed(PRECISION.locations)}`
    db.locations.put({
      ...lonLat,
      locationId,
    })
  })
}

export const getLocationForLonLat = async (lonLat: LonLat): Promise<string> => {
  if (!lonLat) return ''
  const locationId = `lon:${lonLat[0].toFixed(
    PRECISION.locations
  )},lat:${lonLat[1].toFixed(PRECISION.locations)}`
  const location = await db.locations.get(locationId)
  return location?.address_line1
    ? `near ${location?.address_line1}, ${location?.address_line2}`
    : ''
}

export const getHorizonInfo = async (
  marker: string | boolean,
  height: string | false
): Promise<{ text: string; distanceKm: number }> => {
  if (marker !== 'here' && marker !== 'horizonMarker')
    return { text: '', distanceKm: 0 }

  let activeAutoTrack = await getActiveAutoTrack()
  if (!activeAutoTrack) return { text: '', distanceKm: 0 }
  let lastPoint = activeAutoTrack.points[0]

  // Calculate my height above ground
  let extraHeight = 1.7 // Body height until eyes
  if (height) {
    const isFloors = height.slice(-1) === 'f'
    const heightInt = parseInt(height)
    extraHeight += isFloors ? heightInt * 3 : heightInt
  }

  const viewHeight = (lastPoint.altitude || 0) + extraHeight
  const earthDiameter = 12713000 // 12713 km
  let horizonDistance = Math.sqrt((earthDiameter + viewHeight) * viewHeight)
  horizonDistance = Math.round(horizonDistance / 100) / 10
  return {
    distanceKm: horizonDistance,
    text: `Horizon ${
      viewHeight
        ? `at ${Math.round(lastPoint.altitude || 0) + ' m alt '} and ${
            Math.round(extraHeight) + ' m view height'
          } is`
        : 'at sealevel'
    } ${horizonDistance} km`,
  }
}
