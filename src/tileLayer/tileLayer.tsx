import { useEffect, useRef, useState } from 'react'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { useLiveQuery } from 'dexie-react-hooks'
import OLTileLayer from 'ol/layer/Tile'
import { fromLonLat, toLonLat } from 'ol/proj'
import { OSM, Vector } from 'ol/source'
import OLFeature from 'ol/Feature'
import Point from 'ol/geom/Point'
import OLVectorLayer from 'ol/layer/Vector'
import { Circle, Fill, Style } from 'ol/style'
import { getTile } from './tileLayer.utils'
import { logsState, MapCenter, mapCenterState } from '../store/store'
import { DEFAULT_ZOOM, MAX_AGE_BEARING, palette, TILE_URL } from '../config'
import { log } from '../common/utils'
import { AppStatus, db } from '../store/db'
// import { Coordinate } from 'ol/coordinate'
// import { Layer } from 'ol/layer'
import {
  setToMarkers,
  setDotAt,
  setNorthMarker,
} from '../trackLayer/trackLayer.utils'
import { getLocationForLonLat } from '../header/header.api'
import { useInterval, useTimeout } from '@chakra-ui/react'
import { mapOnMoveEnd, mapOnClick } from './tileLayer.events'

interface TileLayerProps {
  map: any
  domTargetId: string
  zIndex: number
}

const TileLayer = (props: TileLayerProps) => {
  const { map, domTargetId, zIndex } = props
  const setLogs = useSetRecoilState(logsState)
  const appStatusDexie = useLiveQuery(() => db.appStatus.toCollection().last())
  const [mapCenter, setMapCenter] = useRecoilState(mapCenterState)

  // Define the source: attach thunderforest tiles to OSM code
  const source = new OSM({
    url: TILE_URL,
  })

  const haveSetUpMap = useRef(false)

  // Effect to set up the setup the tiles and features when we have a map object
  useEffect(() => {
    if (!map) return
    if (!source) return
    if (!domTargetId) return

    // We only need to do this once
    if (haveSetUpMap.current) return
    haveSetUpMap.current = true

    // We need to set the target div IN the tileLayer instead of where map is created. Strange!
    map.setTarget(domTargetId)

    // Our custom tile loader - this will store maps on the device
    source.setTileLoadFunction(getTile)

    // Create the tile layer
    log('Adding the tiles to the map', setLogs)
    let tileLayer = new OLTileLayer({
      source,
      zIndex,
    })
    map.addLayer(tileLayer)
    tileLayer.setZIndex(zIndex)

    // Set up map listeners for map click and map move
    mapOnClick(map, setLogs)
    mapOnMoveEnd(map, setMapCenter)

    // Reinstate the mapcenter, Need the timeout to allow the map to fully load before setting params
    setTimeout(async () => {
      setMapCenter(mapCenter)

      // Get the current location and mark it with a red dot (but dont move there)
      await db.appStatus.update('TrackTracker', { getHere: 'get' })
    }, 500)

    // preLoadAndReport() - For now no preloading
  }, [map, source, domTargetId])

  // Effect to recenter + rezoom the map when we get a new coord
  useEffect(() => {
    if (!map) return
    if (!mapCenter) return
    if (mapCenter.lonLat[0] === 0 && mapCenter.lonLat[1] === 0) return
    if (!mapCenter.lonLat[0] || !mapCenter.lonLat[1]) return // todo: replace this with lodash thing for null | undefined

    // Set the map center
    const mapCenterArray = mapCenter.lonLat.toString().split(',')
    const view = map.getView()
    view.setCenter(fromLonLat(mapCenter.lonLat))

    // Set a marker at mapCenter
    setDotAt(map, mapCenter.lonLat, mapCenter.dotName)

    // Set the map zoom level
    const zoomLevel = mapCenter.zoomLevel || DEFAULT_ZOOM
    view.setZoom(parseInt(zoomLevel.toString()))

    const msg = `Setting to ${
      mapCenterArray[0].slice(0, 10) + ', ' + mapCenterArray[1].slice(0, 10)
    } at zoom-level ${zoomLevel}`
    log(msg)

    // Get location for these coords
    getLocationForLonLat(mapCenter.lonLat).then((location) => {
      if (location) log(`Set to ${location}`)
    })

    // preLoadAndReport() - For now no preloading
  }, [mapCenter])

  // Effect to set the North marker on device turn
  useEffect(() => {
    setNorthMarker(map, mapCenter, appStatusDexie?.deviceOrientation)
  }, [appStatusDexie?.deviceOrientation])

  // If the bearing is too old, invalidate the value
  // TODO use this as well to check for internet status
  useInterval(() => {
    console.log('after 3 sec interval', appStatusDexie?.deviceOrientation)
    if (!appStatusDexie?.deviceOrientation) return
    if (!appStatusDexie.deviceOrientation.isValid) {
      checkForSimulators(appStatusDexie)
      return
    }
    const ageInMs = Date.now() - appStatusDexie.deviceOrientation.time
    if (ageInMs < MAX_AGE_BEARING * 1000) return

    // Mark the bearing as invalid
    const newOrientation = { ...appStatusDexie.deviceOrientation }
    newOrientation.isValid = false

    // Check for simulators
    checkForSimulators(appStatusDexie)

    db.appStatus.update('TrackTracker', {
      deviceOrientation: newOrientation,
    })
  }, 3000)

  // Effect to set the Destination ("to") marker
  useEffect(() => {
    if (appStatusDexie?.to?.lonLat) {
      setToMarkers(
        map,
        mapCenter,
        appStatusDexie.to.lonLat,
        appStatusDexie.deviceOrientation
      )
    } else {
      // Remove the To markers
      setToMarkers(map, mapCenter, undefined, undefined)
    }
  }, [appStatusDexie?.to?.lonLat])

  const checkForSimulators = (appStatus: AppStatus) => {
    if (isNaN(appStatus.deviceOrientation.bearing)) {
      const newOrientation = { ...appStatus.deviceOrientation }
      newOrientation.bearing = 0
      newOrientation.alpha = 0
      newOrientation.bearings = {
        absolute: 0,
        absoluteTime: 0,
        absoluteIsValid: false,
        relative: 11,
        relativeTime: 0,
        relativeOffset: 0,
        relativeIsValid: false,
      }
      db.appStatus.update('TrackTracker', {
        deviceOrientation: newOrientation,
      })
    }
  }

  /* Preload a map and lower zoomlevels - for now not using this
  const preLoadAndReport = () => {
    // This is a good place to get the total of preloaded tiles for THIS screen
    // Will the preload make a report for the correct extent?
    // How do i visualise an extent?
    // For now, forget preloading zoom levels
    return

    // Load the lower zoom levels
    // let maxCount = 10
    // let carryOn = true
    // while (carryOn && maxCount-- > 0) {
    //   const {
    //     storedPercentage,
    //     zoom,
    //     logAdditions: newLogs,
    //   } = preloadZoomLevels(map, source, 16)
    //   setLogs((oldLogs) => newLogs.concat(oldLogs))
    //   setAppStatus({ storedPercentage, zoom })
    //
    //   log('StoredPercentage is now: ' + storedPercentage, setLogs)
    //   carryOn = storedPercentage < 100
    // }
  }
  */
  return null
}

export default TileLayer
