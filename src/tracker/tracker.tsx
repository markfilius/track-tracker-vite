import * as React from 'react'
import { useEffect, useRef, useState } from 'react'
import * as ol from 'ol'
import { fromLonLat, get } from 'ol/proj'
import { useSetRecoilState } from 'recoil'
import TileLayer from '../tileLayer/tileLayer'
import TrackLayer from '../trackLayer/trackLayer'
import './tracker.css'
import { logsState } from '../store/store'
import { BRISBANE_LONLAT, DEFAULT_ZOOM } from '../config'
import { log } from '../common/utils'

export const Tracker = () => {
  const hasMountedRef = useRef(false)
  const [map, setMap] = useState<any>(null)
  const setLogs = useSetRecoilState(logsState)

  // Effect to create a map on load
  useEffect(() => {
    if (hasMountedRef.current) return
    hasMountedRef.current = true

    let options = {
      view: new ol.View({
        zoom: DEFAULT_ZOOM,
        center: fromLonLat(BRISBANE_LONLAT),
      }),
      layers: [],
      controls: [],
      overlays: [],
    }

    log('Starting the app', setLogs)
    // TODO: What type is this? to replace the anys
    let pluggableMap = new ol.Map(options)
    setMap(pluggableMap)
  }, [])

  return (
    <div id="theMap" className="theMapContainer">
      <div className="mapLayers">
        <TileLayer map={map} zIndex={1} domTargetId={'theMap'} />
        <TrackLayer map={map} zIndex={2} />
      </div>
    </div>
  )
}
