import { atom, selector } from 'recoil'
import { recoilPersist } from 'recoil-persist'
import { SearchedLocation } from './db'
import { DEFAULT_ZOOM } from '../config'

const { persistAtom } = recoilPersist({
  key: 'recoil-persist',
  storage: localStorage,
})

export interface LogItem {
  id: string
  repeat: number
  time: string
  data: any
}

export const logsState = atom<LogItem[]>({
  key: 'logs',
  default: [],
  effects_UNSTABLE: [persistAtom],
})

export type LonLat = [lon: number, lat: number]

export type MapCenter = {
  lonLat: LonLat
  zoomLevel: number
  dotName: 'here' | 'location' | ''
}

export const mapCenterState = atom<MapCenter>({
  key: 'mapCenter',
  default: { lonLat: [0, 0], dotName: '', zoomLevel: DEFAULT_ZOOM },
  effects_UNSTABLE: [persistAtom],
})

export const zoomLevelState = atom<number>({
  key: 'zoomLevel',
  default: 11,
  effects_UNSTABLE: [persistAtom],
})

export const searchResultsState = atom<SearchedLocation[]>({
  key: 'searchResults',
  default: [],
  effects_UNSTABLE: [persistAtom],
})

// Not used ??????
// export type AppStatus = {
//   storedPercentage: number
//   zoom: number
//   isLoading: boolean
//   hasTileLoadError: boolean
//   getHere: boolean
//   hasInternet: boolean
//   headerMessage: string
// }
//
// export const appStatusState = atom<AppStatus>({
//   key: 'appStatus',
//   default: {
//     storedPercentage: 0,
//     zoom: 0,
//     isLoading: false,
//     hasTileLoadError: false,
//     getHere: false,
//     hasInternet: false,
//     headerMessage: '',
//   },
//   effects_UNSTABLE: [persistAtom],
// })

// TODO FROM THE DOCO - USE THIS ISO RECOIL-PERSIST - OR - SYNC TO INDEXED-DB
// const localStorageEffect =
//   (key) =>
//   ({ setSelf, onSet }) => {
//     const savedValue = localStorage.getItem(key)
//     if (savedValue != null) {
//       setSelf(JSON.parse(savedValue))
//     }
//
//     onSet((newValue, _, isReset) => {
//       isReset
//         ? localStorage.removeItem(key)
//         : localStorage.setItem(key, JSON.stringify(newValue))
//     })
//   }
//
// const currentUserIDState = atom({
//   key: 'CurrentUserID',
//   default: 1,
//   effects: [
//     localStorageEffect('current_user'),
//   ]
// });
