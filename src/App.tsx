import { RecoilRoot } from 'recoil'
import { Box, ChakraProvider, extendTheme } from '@chakra-ui/react'
import './App.css'
import { Tracker } from './tracker/tracker'
import { Header } from './header/header'

function App() {
  // On initial load, reset the stored stats
  console.log('resetting stats')
  localStorage.setItem('cachedNrTiles', '0')
  localStorage.setItem('fetchedNrTiles', '0')

  const customTheme = extendTheme({
    // semanticTokens: {
    //   // is this used ?????
    //   colors: {
    //     error: 'red.500',
    //     text: {
    //       default: 'red.900',
    //     },
    //     heavyBackground: 'pink', /// ??????
    //   },
    // },
  })

  return (
    <RecoilRoot>
      <ChakraProvider theme={customTheme}>
        <Box className={'appContainer'}>
          <Header />
          <Tracker />
        </Box>
      </ChakraProvider>
    </RecoilRoot>
  )
}

export default App
