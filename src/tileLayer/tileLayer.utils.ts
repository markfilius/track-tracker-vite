// @ts-ignore
import { Tile } from 'ol'
import * as olProj from 'ol/proj'
import { OSM } from 'ol/source'
import { DEFAULT_ZOOM } from '../config'
import { makeNewLogItem } from '../common/utils'
import { LogItem } from '../store/store'
import { db } from '../store/db'

type PreloadResult = {
  storedPercentage: number
  zoom: number
  logAdditions: LogItem[]
}

//************* Custom functions for openlayers ********
// The customised load function passed to openlayers
export const getTile = async (tile?: Tile, url?: string) => {
  if (!url) return
  if (!tile) return

  if (await getFromCache(url)) {
    processZoomLevel(url)
    processStoredStats('cached')
    // @ts-ignore - getImage is not on tile
    ;(tile.getImage() as HTMLImageElement).src = url
    return
  }

  // If there's no internet, don't attempt to load tiles
  const appStatusDexie = await db.appStatus.get('TrackTracker')
  if (!appStatusDexie?.haveInternet) return

  console.log('loading url from internet', url)

  window.dispatchEvent(new Event('tileLoadStart'))
  try {
    processZoomLevel(url)
    processStoredStats('fetched')
    const imageSrc = await fetchAndCacheTile(url)
    if (imageSrc) {
      // @ts-ignore - getImage is not on tile
      ;(tile.getImage() as HTMLImageElement).src = imageSrc
    }
  } catch (error) {
    console.error('2. Tile fetch failed:', error)
  }
}

// Actual fetch initiated from openlayers > getTile
const fetchAndCacheTile = (url: string): Promise<string | undefined> => {
  return new Promise((resolve, reject) => {
    const mapId = getMapId(url)

    const xhr = new XMLHttpRequest()
    xhr.responseType = 'blob'
    xhr.addEventListener('loadend', function (e) {
      var data = this.response
      if (data) {
        const imageSrc = URL.createObjectURL(data)
        storeTile(mapId, JSON.stringify(imageSrc))

        resolve(imageSrc)
      } else {
        const message = 'No image return for the map. Possibly a limit reached'
        console.error('3. Generic error on tile load:', message, data)
        reject(message)
      }
    })
    xhr.addEventListener('error', function (e) {
      // Notify that there are load errors
      window.dispatchEvent(new Event('tileLoadError'))
      const message = '1. Unexpected error while getting map images'
      console.error(message, e)
      reject(message)
    })
    xhr.open('GET', url)
    xhr.send()
  })
}
//******************************************************

//************* Our utils ******************************
export const storeTile = (id: string, tileSrc: string): void => {
  db.tiles.add({ tileId: id, tileSrc })
}

export const retrieveTile = async (id: string): Promise<string> => {
  const tile = await db.tiles.get(id)
  return tile?.tileSrc || ''
}

// // Used to preload into cache deeper zoom levels
// export const preloadZoomLevels = (
//   map: any,
//   source: OSM,
//   maxZoom: number = 16
// ): PreloadResult => {
//   // console.log('preloadZoomLevels called')
//   if (!map) return { storedPercentage: 0, zoom: 0, logAdditions: [] }
//   if (!source) return { storedPercentage: 0, zoom: 0, logAdditions: [] }
//
//   const grid = source.getTileGrid()
//   if (!grid) return { storedPercentage: 0, zoom: 0, logAdditions: [] }
//
//   const currentZoom = Math.floor(map.getView().getZoom() || DEFAULT_ZOOM)
//   const zoomStep = 2
//   const tileUrlFunction = source.getTileUrlFunction()
//   const extent = map.getView().calculateExtent(map.getSize())
//
//   let logAdditions: LogItem[] = []
//   logAdditions = makeNewLogItem(
//     `will get all tiles at all zooms (from ${currentZoom} to ${maxZoom} in steps of ${zoomStep})`,
//     logAdditions
//   )
//
//   // This is nasty & relies on outer scope vars - fix this!
//   const getTheTileInfoAndStore = async (maxCalls: number) => {
//     let didFetchandStore = 0
//     let tileZoomCount = 0
//     let wasInCacheAlready = 0
//
//     for (let z = currentZoom; z <= maxZoom; z = z + zoomStep) {
//       grid.forEachTileCoord(extent, z, function (tileCoord: number[]) {
//         tileZoomCount++
//
//         const projection = olProj.get('EPSG:3857')
//         if (!projection) return
//         const url = tileUrlFunction(tileCoord, 1, projection)
//         if (!url) return
//
//         // If not in from cache, get and store it in cache
//         if (getFromCache(url)) {
//           wasInCacheAlready++
//         } else if (maxCalls-- > 0) {
//           getTile(undefined, url)
//           didFetchandStore++
//         }
//       })
//     }
//
//     return {
//       totalTiles: tileZoomCount,
//       wasInCache: wasInCacheAlready,
//       nowInCache: wasInCacheAlready + didFetchandStore,
//       didFetch: didFetchandStore,
//     }
//   }
//   const stats = getTheTileInfoAndStore(300)
//   logAdditions = makeNewLogItem(
//     `Stored another ${stats.didFetch} tiles (of ${
//       stats.totalTiles
//     } to ${Math.round(
//       (stats.nowInCache * 100) / stats.totalTiles
//     )}%)(was in cache ${stats.wasInCache})`,
//     logAdditions
//   )
//
//   return {
//     storedPercentage: Math.round((stats.nowInCache * 100) / stats.totalTiles),
//     zoom: currentZoom,
//     logAdditions,
//   }
// }

const getMapId = (url: string): string => {
  const mapIdArray = url.slice(9).split('/')
  mapIdArray.shift()
  const mapIdApi = mapIdArray.join('/')
  const mapId = mapIdApi.split('?')[0]
  return mapId
}

const getFromCache = async (url: string): Promise<string> => {
  const mapId = getMapId(url)
  const tile = await retrieveTile(mapId)
  // console.log('retreiving', await retrieveTile(mapId))
  return tile || ''
}

const processZoomLevel = (url: string) => {
  // console.log('processZoomLevel', url)
  const urlParts = url.split('/outdoors/')
  const zoomLevelFromUrl = urlParts[1].split('/')[0]
  const storedZoomLevel = localStorage.getItem('zoomLevel')
  if (storedZoomLevel === zoomLevelFromUrl) return

  // We have changed zoom, so store and send event
  localStorage.setItem('zoomLevel', zoomLevelFromUrl)
  const zoomLevelEvent = new Event('zoomLevelChanged')
  window.dispatchEvent(zoomLevelEvent)
}

const processStoredStats = (source: 'cached' | 'fetched') => {
  let cachedNrTiles = parseInt(localStorage.getItem('cachedNrTiles') || '0')
  let fetchedNrTiles = parseInt(localStorage.getItem('fetchedNrTiles') || '0')
  if (source === 'cached') cachedNrTiles += 1
  if (source === 'fetched') fetchedNrTiles += 1
  localStorage.setItem('cachedNrTiles', cachedNrTiles.toString())
  localStorage.setItem('fetchedNrTiles', fetchedNrTiles.toString())

  const oldPercentage = localStorage.getItem('storedTilesPercentage')
  const newPercentage = Math.round(
    (cachedNrTiles * 100) / (cachedNrTiles + fetchedNrTiles)
  ).toString()

  localStorage.setItem('storedTilesPercentage', newPercentage)
  const storedTilesPercentageEvent = new Event('storedTilesPercentageChanged')
  window.dispatchEvent(storedTilesPercentageEvent)
}
