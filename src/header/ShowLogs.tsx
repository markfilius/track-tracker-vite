import { Text, Box, IconButton } from '@chakra-ui/react'
import { CloseIcon } from '@chakra-ui/icons'
import { MAX_LOGS, palette } from '../config'
import { useLiveQuery } from 'dexie-react-hooks'
import { db } from '../store/db'
import './ShowLogs.css'

export const ShowLogs = (props: { onClick: () => void }) => {
  // Get the logs - limited and in reverse,
  const logsDex = useLiveQuery(() => {
    return db.logItems.reverse().limit(MAX_LOGS).toArray()
  })

  const displayLogs = () => {
    if (!logsDex) return null
    return logsDex.map((log, index) => {
      return (
        <p key={(log?.id?.toString() ?? '') + log.time + index.toString()}>
          {log.time}: {log.data}
          {log.repeat > 1 ? ` [${log.repeat}x]` : ''}
        </p>
      )
    })
  }

  return (
    <Box className={'showLogsContainer'} onClick={props.onClick}>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Text as={'i'} color={palette.titleSec}>
          System logs
        </Text>

        {/* Close icon */}
        <IconButton
          colorScheme={palette.action}
          aria-label="Close Search"
          icon={<CloseIcon />}
          size={'xs'}
          onClick={props.onClick}
        />
      </Box>

      {displayLogs()}
    </Box>
  )
}
