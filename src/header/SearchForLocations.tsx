import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import { useState } from 'react'
import {
  Box,
  Button,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  Text,
} from '@chakra-ui/react'
import { CloseIcon, SpinnerIcon } from '@chakra-ui/icons'
import { MAX_PREVIOUS_LOCATIONS, palette, PRECISION } from '../config'
import { log } from '../common/utils'
import {
  LogItem,
  logsState,
  mapCenterState,
  searchResultsState,
} from '../store/store'
import { fetchLocationCoords, storeLonLatToLocation } from './header.api'
import { db, SearchedLocation } from '../store/db'
import './SearchForLocations.css'
import { useLiveQuery } from 'dexie-react-hooks'

// TODO allow to search wider than aus

export const SearchForLocation = (props: { onClick: () => void }) => {
  const logs = useRecoilValue<LogItem[]>(logsState)
  const setLogs = useSetRecoilState(logsState)
  const appStatusDexie = useLiveQuery(() => db.appStatus.toCollection().last())
  const [searchResults, setSearchResults] = useRecoilState(searchResultsState)
  const [mapCenter, setMapCenter] = useRecoilState(mapCenterState)
  const [getCoordsFor, setGetCoordsFor] = useState('')
  const [isSearching, setIsSearching] = useState(false)

  // Get the previous searches - limited and in reverse,
  const previousLocationsDex = useLiveQuery(() => {
    return db.searches.reverse().limit(MAX_PREVIOUS_LOCATIONS).toArray()
  })

  const handleSearchFor = (event: any) => {
    log('handleSearchFor ' + event.target.value, setLogs)
    setGetCoordsFor(event.target.value)
  }

  const startSearch = () => {
    log('start search: ' + getCoordsFor, setLogs)
    getLocationCoords()
  }

  const getLocationCoords = async () => {
    setIsSearching(true)
    try {
      const results: SearchedLocation[] = await fetchLocationCoords(
        getCoordsFor
      )
      log('Found coords: ' + results.toString(), setLogs)
      const foundLocations = results || []

      // Deduplicate any results (within approx PRECISION.searches accuracy, where 3rd decimal is approx 100m)
      const seenLonLats: string[] = []
      const deduplicatedLocations: SearchedLocation[] = []
      for (const location of foundLocations) {
        const approxLonLat = `${location.lon.toFixed(
          PRECISION.searches
        )},${location.lat.toFixed(PRECISION.searches)}`
        if (seenLonLats.includes(approxLonLat)) continue
        deduplicatedLocations.push(location)
        seenLonLats.push(approxLonLat)
      }

      // Store a reverse lookup: lonLat to location name
      storeLonLatToLocation(deduplicatedLocations)

      setSearchResults(deduplicatedLocations)
    } catch (error) {
      // @ts-ignore - error is type unknown
      log('get coords for: error: ' + error.message, setLogs)
    }
    setIsSearching(false)
  }

  const handleSelectLocation = async (
    placeId: string,
    fromPrevious = false
  ) => {
    const source = fromPrevious ? previousLocationsDex : searchResults
    const selected = source?.find((location) => location.place_id === placeId)
    if (!selected) return

    log(
      `We want: ${selected.address_line1} at ${selected.lon}, ${selected.lat}`,
      setLogs
    )
    setMapCenter({
      lonLat: [selected.lon, selected.lat],
      dotName: 'location',
      zoomLevel: mapCenter.zoomLevel,
    })

    // Add to the db
    if (fromPrevious) return
    try {
      const logId = await db.searches.add(selected)
    } catch (error) {
      console.log('dexxie error', error)
    }
  }

  return (
    <Box className={'searchContainer'}>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        {/* Search box */}
        <InputGroup w={'90%'}>
          <Input
            ml={5}
            pr="10"
            type={'text'}
            placeholder="Search within Australia"
            onChange={handleSearchFor}
            disabled={!appStatusDexie?.haveInternet}
          />
          <InputRightElement width="10">
            {isSearching ? (
              <IconButton aria-label="Search database" icon={<SpinnerIcon />} />
            ) : (
              <Button
                h={'80%'}
                onClick={startSearch}
                disabled={isSearching || !appStatusDexie?.haveInternet}
                colorScheme={palette.action}
              >
                {'Go'}
              </Button>
            )}
          </InputRightElement>
        </InputGroup>

        {/* Close icon */}
        <IconButton
          colorScheme={palette.action}
          aria-label="Close Search"
          icon={<CloseIcon />}
          size={'xs'}
          onClick={props.onClick}
        />
      </Box>

      {/* Search results */}
      <Box className={'searchResultsContainer'} onClick={props.onClick}>
        <Text as={'i'} color={palette.titleSec}>
          Recent Search Results
        </Text>
        {searchResults.length ? (
          searchResults.map((location, index) => {
            return (
              <Box
                key={location.place_id + index}
                onClick={() => handleSelectLocation(location.place_id)}
              >
                <Text as={'u'} fontSize={'lg'}>
                  {location.address_line1}
                </Text>
                <Text as={'i'} fontSize={'sm'}>
                  {', ' + location.address_line2}
                </Text>
              </Box>
            )
          })
        ) : (
          <p>No locations found</p>
        )}
      </Box>

      {/* Previous results */}
      <Box className={'searchResultsContainer'} onClick={props.onClick}>
        <Text as={'i'} color={palette.titleSec}>
          Previously Selected Locations
        </Text>
        {previousLocationsDex && previousLocationsDex.length ? (
          previousLocationsDex.map((location, index) => {
            return (
              <Box
                key={location.place_id + index}
                onClick={() => handleSelectLocation(location.place_id, true)}
              >
                <Text as={'u'} fontSize={'lg'}>
                  {location.address_line1}
                </Text>
                <Text as={'i'} fontSize={'sm'}>
                  {', ' + location.address_line2}
                </Text>
              </Box>
            )
          })
        ) : (
          <p>No previously selected locations</p>
        )}
      </Box>
    </Box>
  )
}
