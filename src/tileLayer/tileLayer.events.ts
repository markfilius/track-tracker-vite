import { log } from '../common/utils'
import { db } from '../store/db'
import { toLonLat } from 'ol/proj'
import { LogItem, MapCenter } from '../store/store'

export const mapOnClick = (
  map: any,
  setLogs: (
    valOrUpdater: ((currVal: LogItem[]) => LogItem[]) | LogItem[]
  ) => void
) => {
  map.on('singleclick', (event: any) => {
    const features = map
      .getFeaturesAtPixel(event.pixel)
      .map((f: any) => f.values_?.name || '')

    log(
      `Single click on the map${
        features.length ? ', features: ' + features.join(', ') : ''
      }`,
      setLogs
    )

    db.appStatus.update('TrackTracker', {
      coordsOnClick: toLonLat(event.coordinate),
      altitudeOnClick: event.altitude,
      featuresOnClick: features,
    })
  })
}

export const mapOnMoveEnd = (
  map: any,
  setMapCenter: (valOrUpdater: any) => void
) => {
  map.on('moveend', (data: any) => {
    const newCenter = map.getView().getCenter()
    if (newCenter) {
      // const newCenterArray = toLonLat(newCenter).toString().split(',')
      // const newCenterString =
      //   newCenterArray[0].slice(0, 10) + ', ' + newCenterArray[1].slice(0, 10)

      setMapCenter({
        lonLat: toLonLat(newCenter),
        dotName: '',
        zoomLevel: Math.floor(map.getView().getZoom()),
      } as MapCenter)

      db.appStatus.update('TrackTracker', {
        coordsOnClick: undefined,
        altitudeOnClick: undefined,
      })
    }
  })
}
