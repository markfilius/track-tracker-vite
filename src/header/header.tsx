import { useEffect, useRef, useState } from 'react'
import { useLiveQuery } from 'dexie-react-hooks'
import { Coordinate } from 'ol/coordinate'
import {
  Box,
  Button,
  IconButton,
  Input,
  Text,
  useInterval,
} from '@chakra-ui/react'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import isOnline from 'is-online'
import packageJson from '../../package.json'
import { LogItem, logsState, LonLat, mapCenterState } from '../store/store'
import './header.css'
import { log } from '../common/utils'
import { db, DeviceOrientation } from '../store/db'
import { SearchForLocation } from './SearchForLocations'
import { ShowLogs } from './ShowLogs'
import { CloseIcon, WarningTwoIcon } from '@chakra-ui/icons'
import { palette, PRECISION } from '../config'
import { debounce } from 'lodash'
import {
  deviceOrientationListeners,
  getHereListeners,
  onlineListeners,
  storeTilesListeners,
  tileLoadListeners,
} from './header.events'
import { getHorizonInfo, getLocationForLonLat } from './header.api'
import { setToMarkers } from '../trackLayer/trackLayer.utils'

export const Header = () => {
  const logs = useRecoilValue<LogItem[]>(logsState)
  const setLogs = useSetRecoilState(logsState)
  const [mapCenter] = useRecoilState(mapCenterState)
  const appStatusDexie = useLiveQuery(() => db.appStatus.toCollection().last())
  const [storedTilesPercentageChanged, setStoredTilesPercentageChanged] =
    useState(0)
  const [headerPanelIsOpen, setHeaderPanelIsOpen] = useState(false)
  const [showLogsIsOpen, setShowLogsIsOpen] = useState(false)
  const [searchIsOpen, setSearchIsOpen] = useState(false)
  const [herePanelIsOpen, setHerePanelIsOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [gettingPosition, setGettingPosition] = useState(false)
  const [gettingPositionError, setGettingPositionError] = useState(false)
  const [formattedLocation, setFormattedLocation] = useState('')
  const [heightSetter, setHeightSetter] = useState<string | false>('')
  const [horizonInfo, setHorizonInfo] = useState('')
  const [hasLoadError, setHasLoadError] = useState(false)
  const loadTimer = useRef(0)
  const [startCountDown, setStartCountDown] = useState(false)
  const [countDown, setCountDown] = useState(10)

  const lastCoord = useRef([0, 0])

  // Set up the event listeners
  useEffect(() => {
    deviceOrientationListeners(setLogs)
    tileLoadListeners(setLogs, setHasLoadError, setIsLoading, loadTimer)
    storeTilesListeners(setStoredTilesPercentageChanged)
    getHereListeners(
      setGettingPositionError,
      setGettingPosition,
      setStartCountDown,
      setCountDown,
      countDown,
      startCountDown
    )

    window.addEventListener('compassneedscalibration', (d) => {
      log(`compassneedscalibration event fired: ${d}`)
    })
  }, [])

  // Online listener is called on every render
  onlineListeners(appStatusDexie)

  // Effect to close the panels when clicked om the map
  useEffect(() => {
    if (!appStatusDexie || !Array.isArray(appStatusDexie?.coordsOnClick)) return

    // Check it actually different coords (the header renders often)
    if (
      appStatusDexie.coordsOnClick[0] === lastCoord.current[0] &&
      appStatusDexie.coordsOnClick[1] === lastCoord.current[1]
    ) {
      return
    }
    lastCoord.current = appStatusDexie.coordsOnClick

    // Should the "here panel" be open?
    const herePanelVisible =
      searchIsOpen || showLogsIsOpen || headerPanelIsOpen
        ? false
        : (appStatusDexie.featuresOnClick?.length ?? 0) > 0
        ? true
        : !herePanelIsOpen
    setHerePanelIsOpen(herePanelVisible)
    if (herePanelVisible) getFormattedLocation()
    setSearchIsOpen(false)
    setShowLogsIsOpen(false)
    setHeaderPanelIsOpen(false)
  }, [appStatusDexie?.coordsOnClick])

  const getFormattedLocation = async () => {
    if (!appStatusDexie) return
    const location = await getLocationForLonLat(
      appStatusDexie.coordsOnClick as LonLat
    )
    const locationDescription = location ? `Location: ${location} ` : ''

    const marker =
      Array.isArray(appStatusDexie.featuresOnClick) &&
      appStatusDexie.featuresOnClick[0]
    const markerDescription =
      marker === 'here'
        ? ' - Your current location'
        : marker === 'northMarker'
        ? ' - North'
        : marker === 'to'
        ? '- Your "To" destination'
        : marker === 'toMarker'
        ? '- Your "To" direction'
        : marker === 'horizonMarker'
        ? '- The horizon the phone is pointing to'
        : ''

    setFormattedLocation(`${locationDescription}${markerDescription}`)
    setHorizonInfo((await getHorizonInfo(marker, heightSetter)).text)
    setHeightSetter(
      marker === 'here' || marker === 'horizonMarker' ? '' : false
    )
  }

  const handleSetHeight = async (event: any) => {
    const newHeightInt = parseInt(event.target.value)
    if (isNaN(newHeightInt)) {
      setHeightSetter('')
      db.appStatus.update('TrackTracker', { userViewHeight: '' })
      return
    }

    const isFloors = event.target.value.slice(-1) === 'f'
    const newHeight = `${newHeightInt}${isFloors ? 'f' : ''}`
    setHeightSetter(newHeight)
    setHorizonInfo((await getHorizonInfo('here', newHeight)).text)

    // Get my position again to force a horizon marker update
    db.appStatus.update('TrackTracker', {
      userViewHeight: newHeight,
      getHere: 'here',
    })
  }

  // The timer is used only by the getHere functions
  useInterval(
    () => {
      setCountDown(countDown - 1)
    },
    startCountDown ? 1000 : null
  )

  // Some general functions
  const toggleHeaderPanel = () => {
    setHeaderPanelIsOpen(!headerPanelIsOpen)
    setSearchIsOpen(false)
    setShowLogsIsOpen(false)
    setHerePanelIsOpen(false)
  }

  const restart = () => {
    window.location.reload()
  }

  const moveHere = () => {
    db.appStatus.update('TrackTracker', { getHere: 'move' })
  }

  const formatCoordsOrAltitude = (lonLat?: Coordinate | number): string => {
    if (!lonLat) return ''
    if (typeof lonLat === 'object') {
      return `Lon: ${lonLat[0].toFixed(
        PRECISION.formatting
      )} Lat: ${lonLat[1].toFixed(PRECISION.formatting)}`
    }
    return lonLat.toFixed(PRECISION.altitude)
  }

  const formatDeviceBearing = (orientation: DeviceOrientation): string => {
    if (orientation?.bearing || orientation.bearing === 0) {
      const secondsAgo = Math.round((Date.now() - orientation.time) / 1000)
      const minutesAgo = Math.round(secondsAgo / 60)
      const timeAgo =
        minutesAgo >= 2
          ? `${minutesAgo} minutes ago`
          : `${secondsAgo} seconds ago`
      return `${orientation.bearing} degrees, ${
        secondsAgo > 0 ? timeAgo : 'now'
      }`
    }
    return '?'
  }

  // Search stuff
  const toggleSearchBox = () => {
    setSearchIsOpen(!searchIsOpen)
    setShowLogsIsOpen(false)
    setHeaderPanelIsOpen(false)
  }

  // Logs stuff
  const toggleShowLogs = () => {
    setSearchIsOpen(false)
    setHeaderPanelIsOpen(false)
    setShowLogsIsOpen(!showLogsIsOpen)
  }

  // AutoTrack stuff
  const toggleAutoTrack = () => {
    if (!appStatusDexie) return
    const newAutoTrack = appStatusDexie.autoTrack === '10min' ? 'none' : '10min'
    db.appStatus.update('TrackTracker', { autoTrack: newAutoTrack })
  }

  // Destination ("to") stuff
  const setTo = () => {
    setHerePanelIsOpen(false)
    db.appStatus.update('TrackTracker', {
      to: {
        ...appStatusDexie?.to,
        lonLat: appStatusDexie?.coordsOnClick,
      },
    })
  }

  const removeTo = () => {
    setHerePanelIsOpen(false)
    db.appStatus.update('TrackTracker', {
      to: undefined,
    })
  }

  return (
    <>
      <Box className={'headerContainer'}>
        <Box className={'titleArea'} onClick={toggleHeaderPanel}>
          <Box className={'messages'}>
            {gettingPosition
              ? ` Getting GPS position (takes ${countDown} seconds)...`
              : ''}
            {gettingPositionError
              ? 'Error - didnt receive a GPS position.'
              : ''}
            {isLoading ? ' Loading maps...' : ''}
            {hasLoadError ? ' Error loading some map tiles.' : ''}
            {appStatusDexie?.headerMessage || ''}
          </Box>
          <Box className={'icons'}>
            {!appStatusDexie?.haveInternet && (
              <WarningTwoIcon color={palette.warning} />
            )}
          </Box>
        </Box>
        {headerPanelIsOpen && (
          <Box className={'contentArea'}>
            {/* Status area */}
            <Box>
              <Text as={'i'} fontSize={'xs'}>
                Status
                {(appStatusDexie?.deviceOrientation.bearing ||
                  appStatusDexie?.deviceOrientation.bearing === 0) && (
                  <Box ml={5}>
                    Device bearing:{' '}
                    {formatDeviceBearing(appStatusDexie?.deviceOrientation)}
                  </Box>
                )}
                <Box ml={5}>
                  Stored on device: {storedTilesPercentageChanged}% (total
                  result, no zoom)
                </Box>
                <Box ml={5}>Current zoom level: {mapCenter.zoomLevel}</Box>
                {/*<Box ml={5}>Auto-Track: {appStatusDexie?.autoTrack}</Box>*/}
                <Box ml={5}>App version: {packageJson.version}</Box>
              </Text>
            </Box>
            <hr />
            {/* Actions area */}
            <Box>
              <Text as={'i'} fontSize={'xs'}>
                Actions
              </Text>

              {/* Various buttons */}
              <Box ml={5}>
                <Button ml={1} onClick={moveHere} isLoading={gettingPosition}>
                  Here
                </Button>
                <Button onClick={toggleSearchBox}>Search</Button>
                <Button onClick={toggleShowLogs}>Logs</Button>
                <Button ml={1} onClick={restart}>
                  Reload
                </Button>
              </Box>
            </Box>
            <hr />
            {/* Settings area */}
            <Box>
              <Text as={'i'} fontSize={'xs'}>
                Settings
              </Text>
              <br />
              <Text as={'i'} fontSize={'xs'}>
                Body height: {(appStatusDexie?.userBodyHeight || 0).toFixed(2)}{' '}
                m
              </Text>
              <br />
              <Text as={'i'} fontSize={'xs'}>
                Viewing height: {appStatusDexie?.userViewHeight || ''}
              </Text>
            </Box>
            {/*<Box>*/}
            {/*  <Button ml={5} onClick={toggleAutoTrack}>*/}
            {/*    Auto-Track*/}
            {/*  </Button>*/}
            {/*</Box>*/}
          </Box>
        )}
        {herePanelIsOpen && (
          <Box className={'herePanel'}>
            <Text as={'i'} fontSize={'xs'}>
              {appStatusDexie?.coordsOnClick && (
                <Box ml={5} mt={3}>
                  {/* The coords */}
                  {formatCoordsOrAltitude(appStatusDexie?.coordsOnClick)}
                </Box>
              )}
              {appStatusDexie?.altitudeOnClick && (
                <Box ml={5}>
                  Altitude on click:{' '}
                  {formatCoordsOrAltitude(appStatusDexie?.altitudeOnClick)}
                </Box>
              )}
              {/* Description */}
              {formattedLocation && <Box ml={5}>{formattedLocation}</Box>}
              {horizonInfo && <Box ml={5}>{horizonInfo}</Box>}

              {/* Height setter */}
              {heightSetter !== false && (
                <>
                  <Input
                    ml={5}
                    mb={2}
                    size={'sm'}
                    type={'text'}
                    placeholder="Height off ground in m or f"
                    onChange={handleSetHeight}
                  />
                  {heightSetter.slice(-1) === 'f' ? 'floors' : ''}
                </>
              )}
            </Text>
            {appStatusDexie?.coordsOnClick && (
              <Box>
                {/* Close icon */}
                <IconButton
                  aria-label="Remove To"
                  icon={<CloseIcon />}
                  size={'xs'}
                  mr={1}
                  onClick={removeTo}
                />
                <Button mr={3} onClick={setTo}>
                  To
                </Button>
              </Box>
            )}
          </Box>
        )}
      </Box>

      {/* Logs */}
      {showLogsIsOpen && <ShowLogs onClick={toggleShowLogs} />}

      {/* Search box */}
      {searchIsOpen && <SearchForLocation onClick={toggleSearchBox} />}
    </>
  )
}
