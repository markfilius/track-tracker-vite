export const DEFAULT_ZOOM = 11
export const MAX_LOGS = 200
export const MAX_PREVIOUS_LOCATIONS = 20
export const BRISBANE_LONLAT = [153.02809, -27.46794]
export const TILE_API_KEY = 'd8e9f2974a1d4244b6862e21d2d6f693'
export const TILE_URL =
  'https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=' +
  TILE_API_KEY
export const PRECISION = {
  locations: 1,
  searches: 2,
  formatting: 6,
  altitude: 0,
}
export const MAX_AGE_BEARING = 10 // seconds. Min is 3

export const palette = {
  action: '#ff0000', // = red
  actionSec: 'steelblue',
  title: 'black',
  titleSec: 'steelblue',
  heavyBackground: 'yellow',
  lightBackground: 'lightyellow',
  standardBackground: 'white',
  warning: 'orange',
  northMarker: '#ffa500', // = 'orange',
  northMarkerInvalid: '#ffa50066', // orange with hex66 opacity
  northMarkerRel: '#ffff00', // = 'yellow',
  northMarkerInvalidRel: '#ffff0066', // yellow with hex66 opacity
  toMarker: '#800080', // = purple
  toMarkerInvalid: '#80008066', // with hex66 opacity
  horizonMarker: '#ff0000',
  horizonMarkerInvalid: '#ff000066',
}
